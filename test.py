# -*- coding: utf-8 -*
import os, time


# [Content] XML Footer Text
def test_hasHomePageNode():
	# os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./xml/homePage.xml')
	f = open('./xml/homePage.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1
# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_sidebarText():
	os.system('adb shell input tap 100 100')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./xml/sidebar.xml')
	f = open('./xml/sidebar.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1	
	assert xmlString.find('查訂單/退訂退款') != -1
	assert xmlString.find('追蹤/買過/看過清單') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('PChome 旅遊') != -1
	assert xmlString.find('線上手機回收') != -1
	assert xmlString.find('給24h購物APP評分') != -1

# 2. [Screenshot] Side Bar Text
def test_sidebarTextScreen():	
	os.system('adb shell input tap 100 100') 
	time.sleep(2)
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./screenshot/sidebar.png')

# 3. [Context] Categories
def test_categories():
	os.system('adb shell input keyevent 4')
	os.system('adb shell input swipe 500 1000 500 120')	
	time.sleep(3)
	os.system('adb shell input tap 950 250')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./xml/categories.xml')
	f = open('./xml/categories.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
	time.sleep(3)

# 4. [Screenshot] Categories
def test_categoriesScreen():
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./screenshot/Categories.png')
	time.sleep(3)

# 5. [Context] Categories page
def test_categoriesPage():
	os.system('adb shell input tap 950 250')
	os.system('adb shell input tap 280 1700')
	time.sleep(2)
	os.system('adb shell input swipe 600 900 600 500')	
	time.sleep(2)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./xml/categoriespage.xml')
	f = open('./xml/categoriespage.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('通訊') != -1
	assert xmlString.find('數位') != -1
	assert xmlString.find('家電') != -1
	assert xmlString.find('日用') != -1
	assert xmlString.find('食品') != -1
	assert xmlString.find('生活') != -1
	assert xmlString.find('運動戶外') != -1
	assert xmlString.find('美妝') != -1
	assert xmlString.find('衣鞋包錶') != -1
	assert xmlString.find('書店') != -1
	assert xmlString.find('電子票券') != -1


# 6. [Screenshot] Categories page
def test_categoriesPageScreen():
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./screenshot/categoriespage.png')

# 7. [Behavior] Search item “switch”
def test_searchSwitch():
	os.system('adb shell input tap 250 130')
	time.sleep(2)
	os.system('adb shell input text Switch')
	time.sleep(2)
	os.system('adb shell input keyevent 66')
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./xml/switch.xml')
	f = open('./xml/switch.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1


# 8. [Behavior] Follow an item and it should be add to the list
def test_followItem():

	os.system('adb shell input tap 500 500 ')
	time.sleep(1)
	os.system('adb shell input tap 120 1700 ')
	time.sleep(1)
	os.system('adb shell input keyevent 4 ')
	time.sleep(1)
	os.system('adb shell input keyevent 4 ')
	time.sleep(1)
	os.system('adb shell input keyevent 4 ')
	time.sleep(1)
	os.system('adb shell input tap 100 100 ')
	time.sleep(1)
	os.system('adb shell input tap 300 900 ')
	time.sleep(1)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./xml/follow.xml')
	f = open('./xml/follow.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('Switch') != -1

# 9. [Behavior] Navigate tto the detail of item
def test_navigateDetail():
	os.system('adb shell input tap 250 700')
	time.sleep(1)
	os.system('adb shell input swipe 900 700 100 700')	
	time.sleep(3)
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./xml/navigate.xml')
	f = open('./xml/navigate.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('詳') != -1
	assert xmlString.find('情') != -1

# 10. [Screenshot] Disconnetion Screen
def test_disconnectedScreen():	
	os.system('adb shell svc data disable')
	time.sleep(1)
	os.system('adb shell svc wifi disable')
	time.sleep(1)
	os.system('adb shell screencap -p /sdcard/screen.png && adb pull /sdcard/screen.png ./screenshot/disconnect.png')
	time.sleep(5)
	os.system('adb shell svc wifi enable')
	os.system('adb shell svc data enable')

